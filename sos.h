/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef SOS_H
#define SOS_H

#include <gmp.h>
#include <vector>
#include <vertex.h>

class Sos {
public:
    Sos();
    ~Sos();

    void Deter2(mpz_t deter, mpz_t b11, mpz_t b21);
    void Deter3(mpz_t deter, mpz_t b11, mpz_t b12, mpz_t b21, mpz_t b22, mpz_t b31, mpz_t b32);
    void Deter4(mpz_t deter, mpz_t b11, mpz_t b12, mpz_t b13, mpz_t b21, mpz_t b22, mpz_t b23, mpz_t b31, mpz_t b32, mpz_t b33, mpz_t b41, mpz_t b42, mpz_t b43);
    void Deter5(mpz_t deter, mpz_t b11, mpz_t b12, mpz_t b13, mpz_t b14, mpz_t b21, mpz_t b22, mpz_t b23, mpz_t b24, mpz_t b31, mpz_t b32, mpz_t b33, mpz_t b34, mpz_t b41, mpz_t b42, mpz_t b43, mpz_t b44, mpz_t b51, mpz_t b52, mpz_t b53, mpz_t b54);
    void Minor2(std::vector<Vertex> & vertexList, int *a, int *b, int ia, int *res);
    void Minor3(std::vector<Vertex> & vertexList, int *a, int *b, int *c, int *i1, int *i2, int *res);
    void Minor4(std::vector<Vertex> & vertexList, int *a, int *b, int *c, int *d, int *res);
    void Minor5(std::vector<Vertex> & vertexList, int *a, int *b, int *c, int *d, int *e, int *res);
    void onlyMinor4(std::vector<Vertex> & vertexList, int *a, int *b, int *c, int *d, int *res);

};

#endif // SOS_H
