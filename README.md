## Requirements

To compile and execute this code, you will need GMP, the GNU multi-precision library. You can install it using the command:
`sudo apt-get install libgmp3-dev`

---

## Build

The Makefile provided here is auto generated using `qmake`. Running the `make` command should generate an executable called `alphaComplex`. 

---

## Execute

The executable generated can be used to compute weighted alpha complexes for set of spheres given in CRD format. The command to execute is:
`alphaComplex <crd-file> <out-file> <sol-rad> <alpha>`

Where:

 1. `<crd-file>` is the input file in CRD format. A sample CRD file is provided named 1GRM.crd.
 2. `<out-file>` is the output alpha complex.
 3. `<sol-rad>` is the solvent radius which can be added to the atom radii before computation of the alpha complex.
 4. `<alpha>` is the alpha value.

---

## Acknowledgement

We would like to thank Dr. Patrice Koehl for sharing the source code of Proshape, a FORTRAN library for computing the weighted alpha complex. We have used that source code as the basis for this C++ library.

---

## Copyright  

Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
Authors  : Raghavendra Sridharamurthy, Talha Bin Masood

Contact  : talha [AT] iisc.ac.in

Citations: 

1. Raghavendra Sridharamurthy, Talha Bin Masood, Harish Doraiswamy, Siddharth Patel, Raghavan Varadarajan and Vijay Natarajan. "**Extraction of robust voids and pockets in proteins.**" In _Visualization in Medicine and Life Sciences III_. Lars Linsen, Hans-Christian Hege, and Bernd Hamann (Eds.). Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349.
2. Talha Bin Masood, Sankaran Sandhya, Nagasuma Chandra and Vijay Natarajan. "**ChExVis: a tool for molecular channel extraction and visualization.**". _BMC Bioinformatics_, 2015, 16:119.
