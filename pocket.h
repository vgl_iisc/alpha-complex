/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef POCKET_H
#define POCKET_H

#include <vector>
#include <spacefillmeasure.h>
#include <vertex.h>
#include <deluanaycomplex.h>
#include <disjointset.h>
#include <volume.h>
#include <alphacomplex.h>

class Pocket {
private:
    int Rank;
    double PI;
    std::vector<int> pocPersistence;

public:
    int npockets;
    double TotVolume;
    double TotSurfArea;
    std::vector<std::vector<int> > AllPockets;
    int PocketNVertex;
    std::vector<int> PocketVertex;
    std::vector<double> SurfPocket;
    std::vector<double> VolPocket;
    std::vector<int> vertices;
    std::vector<double> Radius2;
    SpaceFillMeasure *spm;
    DisJointSet *unionFind;
    Volume *vol;

    Pocket(std::vector<Vertex> & vertexList, int trcount, int ecount);
    ~Pocket();

    bool InComplex(DeluanayComplex *delcx, std::vector<Vertex> &vertexList, unsigned int ftype, int rank, int i);

    void FindPockets(DeluanayComplex *delcx, std::vector <int> &sortedTet);
    std::vector< std::vector<int> > GetPockets();
    void PocketProperties(DeluanayComplex *delcx, std::vector<Vertex> &vertexList, int rank);
    void MeasurePockets(DeluanayComplex *delcx, std::vector<Vertex> & vertexList);

    void dumpPockets(const char* folder, DeluanayComplex* delcx, std::vector<Vertex> &vertexList, int persistence);
};

#endif // POCKET_H
