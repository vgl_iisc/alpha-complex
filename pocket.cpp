/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "pocket.h"
#include <iostream>

Pocket::Pocket(std::vector<Vertex> & vertexList, int trcount, int ecount) {
    Rank = -1;
    PI = 4 * atan(1);
    spm = new SpaceFillMeasure();
    vol = new Volume(vertexList, vertexList.size(), trcount, ecount);
    unionFind = new DisJointSet();
    Radius2.reserve(vertexList.size());
    for (uint i = 1; i < vertexList.size(); i++) {
        Radius2[i] = pow(vertexList[i].Radius, 2.0);
    }
}

Pocket::~Pocket() {
}

/*!
    \fn Pocket::FindPockets(DeluanayComplex *delcx, std::vector <int> &sortedTet)
 */
void Pocket::FindPockets(DeluanayComplex *delcx, std::vector <int> &sortedTet) {
    int i, j, k, itrig;
    unionFind->Clear();
    uint TetSize = delcx->DeluanayTet.size() - delcx->redundantCount;
    pocPersistence.clear();

    for (i = 0; i < delcx->DeluanayTet.size(); i++) {
        delcx->DeluanayTet[i].PocIndex = -1;
        delcx->DeluanayTet[k].ufKey = -1;
    }

    for (i = TetSize - 1; i > 0; i--) {
        if (delcx->DeluanayTet[sortedTet[i]].Depth == delcx->DeluanayTet.size()) continue;

        if (delcx->DeluanayTet[sortedTet[i]].AlphaStatus != -1) {
            continue;
        }
        delcx->DeluanayTet[sortedTet[i]].ufKey = unionFind->ElementCount();
        unionFind->Add(sortedTet[i]);
    }
    for (i = TetSize - 1; i > 0; i--) {
        if (delcx->DeluanayTet[sortedTet[i]].ufKey == -1) continue; //tet not in the uf structure

        for (j = 1; j <= 4; j++) {
            k = delcx->DeluanayTet[sortedTet[i]].Neighbours[j];

            if (delcx->DeluanayTet[k].ufKey == -1) continue; //neighbour not in uf structure

            itrig = delcx->DeluanayTet[sortedTet[i]].TetLink[j];

            //if (delcx->DeluanayTrigs[itrig].isValid == true) continue;  //useful during the modified filtration*/
            if (delcx->DeluanayTrigs[itrig].AlphaStatus == 1) {
                continue;
            }
            unionFind->Union(unionFind->FindSet(delcx->DeluanayTet[sortedTet[i]].ufKey), unionFind->FindSet(delcx->DeluanayTet[k].ufKey));
        }
    }

    AllPockets = unionFind->Consolidate();
    npockets = AllPockets.size();
    for (i = 0; i < AllPockets.size(); i++) {
        for (j = 0; j < AllPockets[i].size(); j++) {
            int pindex = AllPockets[i][j];
            delcx->DeluanayTet[pindex].ufKey = -1;
            delcx->DeluanayTet[pindex].PocIndex = i;
        }
    }

    //now calculate persistence or extended persistence of each pocket here.
    for (i = 0; i < AllPockets.size(); i++) {
        int persistence;
        if (AllPockets[i].size() == 1) //tet persistence is enough
        {
            int tindex = AllPockets[i][0];
            persistence = delcx->DeluanayTet[tindex].Persistence;
            pocPersistence.push_back(persistence);
        } else { /*
                A tetrahedron is part of the pocket means it is not present in the alpha complex also only we need to check Rho.
                Also InComplex doesn't always work as we have not taken care of filtration modifcation in that function.
                */
            int youngestTrig = 0;
            int oldestTet = 0;
            for (j = 0; j < AllPockets[i].size(); j++) {
                int trindex, tindex;
                tindex = AllPockets[i][j];
                if (oldestTet < delcx->DeluanayTet[tindex].Rho) {
                    oldestTet = delcx->DeluanayTet[tindex].Rho;
                }
                for (k = 1; k < 5; k++) {
                    trindex = delcx->DeluanayTet[tindex].TetLink[k];
                    if (delcx->DeluanayTrigs[trindex].AlphaStatus != 0) //belongs to alpha complex
                    {
                        if (delcx->DeluanayTrigs[trindex].Rho) {
                            if (youngestTrig < delcx->DeluanayTrigs[trindex].Rho) {
                                youngestTrig = delcx->DeluanayTrigs[trindex].Rho;
                            }
                        } else {
                            if (youngestTrig < delcx->DeluanayTrigs[trindex].Mu1) {
                                youngestTrig = delcx->DeluanayTrigs[trindex].Mu1;
                            }
                        }
                    }
                }
            }
            assert(youngestTrig <= oldestTet);
            persistence = oldestTet - youngestTrig;
            pocPersistence.push_back(persistence);
        }
    }
}

/*!
    \fn Pocket::GetPockets()
 */
std::vector< std::vector<int> > Pocket::GetPockets() {
    return AllPockets;
}

bool Pocket::InComplex(DeluanayComplex *delcx, std::vector<Vertex> &vertexList, unsigned int ftype, int rank, int i) {
    switch (ftype) {
        case ALF_VERTEX:
        {
            if (vertexList[i].Rho)
                return (vertexList[i].Rho <= rank);
            else
                return (vertexList[i].Mu1 <= rank);
        }
        case ALF_EDGE:
        {
            if (delcx->DeluanayEdges[i].Rho)
                return (delcx->DeluanayEdges[i].Rho <= rank);
            else
                return (delcx->DeluanayEdges[i].Mu1 <= rank);
        }
        case ALF_TRIANGLE:
        {
            if (delcx->DeluanayTrigs[i].Rho)
                return (delcx->DeluanayTrigs[i].Rho <= rank);
            else
                return (delcx->DeluanayTrigs[i].Mu1 <= rank);
        }
        case ALF_TETRA:
        {
            return (delcx->DeluanayTet[i].Rho <= rank);
        }
    }
    return (false);
}

void Pocket::PocketProperties(DeluanayComplex *delcx, std::vector<Vertex> &vertexList, int rank) {
    int i, j, k, l;
    int ipocket, id, idx, pair1, pair2, pair3, pair4, pair5, pair6;
    int trig1, trig2, trig3, trig4;
    double tetravolume, surf, volume;

    SurfPocket.reserve(npockets + 1);
    VolPocket.reserve(npockets + 1);

    for (i = 1; i < vertexList.size(); i++) {
        Radius2[i] = pow(vertexList[i].Radius, 2.0);
    }

    for (ipocket = 0; ipocket < npockets; ipocket++) {
        for (id = 0; id < AllPockets[ipocket].size(); id++) {
            idx = AllPockets[ipocket][id];

            i = delcx->DeluanayTet[idx].Corners[1];
            j = delcx->DeluanayTet[idx].Corners[2];
            k = delcx->DeluanayTet[idx].Corners[3];
            l = delcx->DeluanayTet[idx].Corners[4];

            trig1 = delcx->DeluanayTet[idx].TetLink[4];
            trig2 = delcx->DeluanayTet[idx].TetLink[3];
            trig3 = delcx->DeluanayTet[idx].TetLink[2];
            trig4 = delcx->DeluanayTet[idx].TetLink[1];

            pair1 = delcx->DeluanayTrigs[trig1].TrigLink[3];
            pair2 = delcx->DeluanayTrigs[trig1].TrigLink[2];
            pair4 = delcx->DeluanayTrigs[trig1].TrigLink[1];
            pair3 = delcx->DeluanayTrigs[trig2].TrigLink[2];
            pair5 = delcx->DeluanayTrigs[trig2].TrigLink[1];
            pair6 = delcx->DeluanayTrigs[trig3].TrigLink[1];

            //find initial volume

            tetravolume = vol->DoTetraVolume(vertexList, i, j, k, l);
            VolPocket[ipocket] += tetravolume;

            if (InComplex(delcx, vertexList, ALF_VERTEX, rank, i)) {
                vol->DoTetraVertex(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_VERTEX, rank, j)) {
                vol->DoTetraVertex(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_VERTEX, rank, k)) {
                vol->DoTetraVertex(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_VERTEX, rank, l)) {
                vol->DoTetraVertex(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }

            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair1)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }
            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair2)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }
            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair3)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }
            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair4)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }
            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair5)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }
            if (InComplex(delcx, vertexList, ALF_EDGE, rank, pair6)) {
                vol->DoTetraEdge(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] += volume;
                SurfPocket[ipocket] -= surf;
            }

            if (InComplex(delcx, vertexList, ALF_TRIANGLE, rank, trig1)) {
                vol->DoTetraTriangle(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_TRIANGLE, rank, trig2)) {
                vol->DoTetraTriangle(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_TRIANGLE, rank, trig3)) {
                vol->DoTetraTriangle(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
            if (InComplex(delcx, vertexList, ALF_TRIANGLE, rank, trig4)) {
                vol->DoTetraTriangle(vertexList, i, j, k, l, 1, &surf, &volume);
                VolPocket[ipocket] -= volume;
                SurfPocket[ipocket] += surf;
            }
        }
    }
    for (i = 0; i < npockets; i++) {
        if (SurfPocket[i] < 0) SurfPocket[i] = 0;
        if (VolPocket[i] < 0) VolPocket[i] = 0;
    }
}

/*!
    \fn Pocket::MeasurePockets(DeluanayComplex *delcx, std::vector<Vertex> & vertexList)
 */
void Pocket::MeasurePockets(DeluanayComplex *delcx, std::vector<Vertex> & vertexList) {
    int i, j, k, l, m, option;
    int ipocket, idx, id, pair1, pair2, pair3, pair4, pair5, pair6;
    int trig1, trig2, trig3, trig4;

    double tetra_volume;
    double ra, rb, rc, rd, ra2, rb2, rc2, rd2;
    double wa, wb, wc, wd;
    double surfa = 0.0, surfb = 0.0, surfc = 0.0;
    double vola = 0.0, volb = 0.0, volc = 0.0, coef;
    double dist1 = 0.0, dist2 = 0.0, dist3 = 0.0, dist4 = 0.0, dist5 = 0.0, dist6 = 0.0;
    double d2_1 = 0.0, d2_2 = 0.0, d2_3 = 0.0, d2_4 = 0.0, d2_5 = 0.0, d2_6 = 0.0;
    double ang1 = 0.0, ang2 = 0.0, ang3 = 0.0, ang4 = 0.0, ang5 = 0.0, ang6 = 0.0;

    double a[4];
    double b[4];
    double c[4];
    double d[4];

    double dsurfa[4][3];
    double dsurfb[4][3];
    double dvola[4][3];
    double dvolb[4][3];

    std::vector<int> index;
    std::vector<int> rank;
    std::vector<double> temp;

    index.reserve(npockets + 1);
    rank.reserve(npockets + 1);
    temp.reserve(npockets + 1);
    SurfPocket.reserve(npockets + 1);
    VolPocket.reserve(npockets + 1);

    option = 0;

    for (i = 1; i < vertexList.size(); i++) {
        Radius2[i] = pow(vertexList[i].Radius, 2.0);
    }

    for (i = 0; i < npockets + 1; i++) {
        SurfPocket[i] = 0.0;
        VolPocket[i] = 0.0;
    }

    for (ipocket = 0; ipocket < npockets; ipocket++) {
        for (id = 0; id < AllPockets[ipocket].size(); id++) {
            idx = AllPockets[ipocket][id];
            i = delcx->DeluanayTet[idx].Corners[1];
            j = delcx->DeluanayTet[idx].Corners[2];
            k = delcx->DeluanayTet[idx].Corners[3];
            l = delcx->DeluanayTet[idx].Corners[4];

            ra = vertexList[i].Radius;
            rb = vertexList[j].Radius;
            rc = vertexList[k].Radius;
            rd = vertexList[l].Radius;

            ra2 = Radius2[i];
            rb2 = Radius2[j];
            rc2 = Radius2[k];
            rd2 = Radius2[l];

            wa = (1.0 / 2.0) * vertexList[i].Weight;
            wb = (1.0 / 2.0) * vertexList[j].Weight;
            wc = (1.0 / 2.0) * vertexList[k].Weight;
            wd = (1.0 / 2.0) * vertexList[l].Weight;

            trig1 = delcx->DeluanayTet[idx].TetLink[4];
            trig2 = delcx->DeluanayTet[idx].TetLink[3];
            trig3 = delcx->DeluanayTet[idx].TetLink[2];
            trig4 = delcx->DeluanayTet[idx].TetLink[1];

            pair1 = delcx->DeluanayTrigs[trig1].TrigLink[3];
            pair2 = delcx->DeluanayTrigs[trig1].TrigLink[2];
            pair4 = delcx->DeluanayTrigs[trig1].TrigLink[1];
            pair3 = delcx->DeluanayTrigs[trig2].TrigLink[2];
            pair5 = delcx->DeluanayTrigs[trig2].TrigLink[1];
            pair6 = delcx->DeluanayTrigs[trig3].TrigLink[1];

            for (m = 1; m <= 3; m++) {
                a[m] = vertexList[i].Coordinates[m];
                b[m] = vertexList[j].Coordinates[m];
                c[m] = vertexList[k].Coordinates[m];
                d[m] = vertexList[l].Coordinates[m];
            }

            spm->Tetra6Dihedral(a, b, c, d, &ang1, &ang2, &ang4, &ang3, &ang5, &ang6);

            tetra_volume = spm->TetraVolume(a, b, c, d);

            VolPocket[ipocket] += tetra_volume;
            //check all vertices of the tetrahedron
            if (vertexList[i].AlphaStatus == 0) {
                coef = 0.5 * (ang1 + ang2 + ang3 - 0.5);
                surfa = 4 * PI * Radius2[i] * coef;
                vola = surfa * vertexList[i].Radius / 3.0;

                SurfPocket[ipocket] += surfa;
                VolPocket[ipocket] -= vola;
            }

            if (vertexList[j].AlphaStatus == 0) {
                coef = 0.5 * (ang1 + ang4 + ang5 - 0.5);
                surfa = 4 * PI * Radius2[j] * coef;
                vola = surfa * vertexList[j].Radius / 3.0;

                SurfPocket[ipocket] += surfa;
                VolPocket[ipocket] -= vola;
            }

            if (vertexList[k].AlphaStatus == 0) {
                coef = 0.5 * (ang2 + ang4 + ang6 - 0.5);
                surfa = 4 * PI * Radius2[k] * coef;
                vola = surfa * vertexList[k].Radius / 3.0;

                SurfPocket[ipocket] += surfa;
                VolPocket[ipocket] -= vola;
            }

            if (vertexList[l].AlphaStatus == 0) {
                coef = 0.5 * (ang3 + ang5 + ang6 - 0.5);
                surfa = 4 * PI * Radius2[l] * coef;
                vola = surfa * vertexList[l].Radius / 3.0;

                SurfPocket[ipocket] += surfa;
                VolPocket[ipocket] -= vola;
            }

            //check all edges
            if (delcx->DeluanayEdges[pair1].AlphaStatus == 1) {
                spm->Distance2(vertexList, i, j, &d2_1);
                dist1 = sqrt(d2_1);

                spm->TwoSphereVol(a, b, ra, ra2, rb, rb2, dist1, d2_1, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang1 * (surfa + surfb);
                VolPocket[ipocket] += ang1 * (vola + volb);
            }

            if (delcx->DeluanayEdges[pair2].AlphaStatus == 1) {
                spm->Distance2(vertexList, i, k, &d2_2);
                dist2 = sqrt(d2_2);

                spm->TwoSphereVol(a, c, ra, ra2, rc, rc2, dist2, d2_2, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang2 * (surfa + surfb);
                VolPocket[ipocket] += ang2 * (vola + volb);
            }

            if (delcx->DeluanayEdges[pair3].AlphaStatus == 1) {
                spm->Distance2(vertexList, i, l, &d2_3);
                dist3 = sqrt(d2_3);

                spm->TwoSphereVol(a, d, ra, ra2, rd, rd2, dist3, d2_3, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang3 * (surfa + surfb);
                VolPocket[ipocket] += ang3 * (vola + volb);
            }

            if (delcx->DeluanayEdges[pair4].AlphaStatus == 1) {
                spm->Distance2(vertexList, j, k, &d2_4);
                dist4 = sqrt(d2_4);

                spm->TwoSphereVol(b, c, rb, rb2, rc, rc2, dist4, d2_4, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang4 * (surfa + surfb);
                VolPocket[ipocket] += ang4 * (vola + volb);
            }

            if (delcx->DeluanayEdges[pair5].AlphaStatus == 1) {
                spm->Distance2(vertexList, j, l, &d2_5);
                dist5 = sqrt(d2_5);

                spm->TwoSphereVol(b, d, rb, rb2, rd, rd2, dist5, d2_5, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang5 * (surfa + surfb);
                VolPocket[ipocket] += ang5 * (vola + volb);
            }

            if (delcx->DeluanayEdges[pair6].AlphaStatus == 1) {
                spm->Distance2(vertexList, k, l, &d2_6);
                dist6 = sqrt(d2_6);

                spm->TwoSphereVol(c, d, rc, rc2, rd, rd2, dist6, d2_6, &surfa, &surfb, &vola, &volb, dsurfa, dsurfb, dvola, dvolb, option);

                SurfPocket[ipocket] -= ang6 * (surfa + surfb);
                VolPocket[ipocket] += ang6 * (vola + volb);
            }

            //Finally check faces
            if (delcx->DeluanayTrigs[trig1].AlphaStatus == 1) {
                spm->ThreeSphereVol(a, b, c, ra, rb, rc, ra2, rb2, rc2, wa, wb, wc, dist1, dist2, dist4, d2_1, d2_2, d2_4,
                        &surfa, &surfb, &surfc, &vola, &volb, &volc);

                SurfPocket[ipocket] += 0.5 * (surfa + surfb + surfc);
                VolPocket[ipocket] -= 0.5 * (vola + volb + volc);
            }

            if (delcx->DeluanayTrigs[trig2].AlphaStatus == 1) {
                spm->ThreeSphereVol(a, b, d, ra, rb, rd, ra2, rb2, rd2, wa, wb, wd, dist1, dist3, dist5, d2_1, d2_3, d2_5,
                        &surfa, &surfb, &surfc, &vola, &volb, &volc);

                SurfPocket[ipocket] += 0.5 * (surfa + surfb + surfc);
                VolPocket[ipocket] -= 0.5 * (vola + volb + volc);
            }

            if (delcx->DeluanayTrigs[trig3].AlphaStatus == 1) {
                spm->ThreeSphereVol(a, c, d, ra, rc, rd, ra2, rc2, rd2, wa, wc, wd, dist2, dist3, dist6, d2_2, d2_3, d2_6,
                        &surfa, &surfb, &surfc, &vola, &volb, &volc);

                SurfPocket[ipocket] += 0.5 * (surfa + surfb + surfc);
                VolPocket[ipocket] -= 0.5 * (vola + volb + volc);
            }

            if (delcx->DeluanayTrigs[trig4].AlphaStatus == 1) {
                spm->ThreeSphereVol(b, c, d, rb, rc, rd, rb2, rc2, rd2, wb, wc, wd, dist4, dist5, dist6, d2_4, d2_5, d2_6,
                        &surfa, &surfb, &surfc, &vola, &volb, &volc);

                SurfPocket[ipocket] += 0.5 * (surfa + surfb + surfc);
                VolPocket[ipocket] -= 0.5 * (vola + volb + volc);
            }
        }
    }
    for (i = 0; i < npockets; i++) {
        if (SurfPocket[i] < 0) SurfPocket[i] = 0;
        if (VolPocket[i] < 0) VolPocket[i] = 0;
    }
}

void Pocket::dumpPockets(const char* folder, DeluanayComplex* delcx, std::vector<Vertex> &vertexList, int persistence) {

    char commandLine[150];
    sprintf(commandLine, "rm -f -r  %s", folder);
    std::cerr << commandLine << std::endl;
    system(commandLine);
    sprintf(commandLine, "mkdir %s", folder);
    std::cerr << commandLine << std::endl;
    system(commandLine);

    FILE *fp;
    std::vector<Vertex> skinList;
    skinList.reserve(delcx->DeluanayTet.size());

    std::vector<int> vertexMap;
    vertexMap.reserve(delcx->DeluanayTet.size());
    for (int i = 0; i < vertexList.size(); i++) {
        vertexMap.push_back(-1);
    }

    for (int i = 0; i < AllPockets.size(); i++) {
        // construct Vertex map for tet file
        for (int i = 0; i < delcx->DeluanayTet.size(); i++) {
            vertexMap[i] = -1;
        }
        int tetVertCount = 0;
        for (int j = 0; j < AllPockets[i].size(); j++) {
            int tetIndex = AllPockets[i][j];
            Tetrahedron* tet = &(delcx->DeluanayTet[tetIndex]);
            for (int k = 1; k < 5; k++) {
                if (vertexMap[tet->Corners[k]] == -1) {
                    vertexMap[tet->Corners[k]] = tetVertCount++;
                }
            }
            Vertex ball = tet->powerVert(vertexList);
            skinList.push_back(ball);
        }
        // write Tet file
        char tetFile[150];
        sprintf(tetFile, "%s/%d.tet", folder, i);
        std::cerr << tetFile << std::endl;
        fp = fopen(tetFile, "w");
        fprintf(fp, "%d %d\n", tetVertCount, AllPockets[i].size());
        for (int j = 0; j < vertexMap.size(); j++) {
            if (vertexMap[j] != -1) {
                Vertex vert = vertexList[j];
                fprintf(fp, "%.3f %.3f %.3f\n", vert.Coordinates[1], vert.Coordinates[2], vert.Coordinates[3]);
            }
        }
        for (int j = 0; j < AllPockets[i].size(); j++) {
            int tetIndex = AllPockets[i][j];
            Tetrahedron tet = delcx->DeluanayTet[tetIndex];
            fprintf(fp, "%d %d %d %d\n", vertexMap[tet.Corners[1]], vertexMap[tet.Corners[2]],
                    vertexMap[tet.Corners[3]], vertexMap[tet.Corners[4]]);
        }
        fclose(fp);

        // write union of balls and skin mesh
        int numBalls = skinList.size();
        switch (numBalls) {
            case 3:
            {
                Vertex vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] - 0.723;
                vert.Coordinates[2] = vert.Coordinates[2] - 1.474;
                vert.Coordinates[3] = vert.Coordinates[3] + 0.148;
                vert.Radius = -1.0;
                skinList.push_back(vert);
                break;
            }
            case 2:
            {
                Vertex vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] - 0.723;
                vert.Coordinates[2] = vert.Coordinates[2] - 1.474;
                vert.Coordinates[3] = vert.Coordinates[3] + 0.148;
                vert.Radius = -1.0;
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] + 0.834;
                vert.Coordinates[2] = vert.Coordinates[2] + 0.487;
                vert.Coordinates[3] = vert.Coordinates[3] - 0.261;
                vert.Radius = -1.0;
                skinList.push_back(vert);
                break;
            }
            case 1:
            {
                Vertex vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] - 0.723;
                vert.Coordinates[2] = vert.Coordinates[2] - 1.474;
                vert.Coordinates[3] = vert.Coordinates[3] + 0.148;
                vert.Radius = -1.0;
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] + 0.834;
                vert.Coordinates[2] = vert.Coordinates[2] + 0.487;
                vert.Coordinates[3] = vert.Coordinates[3] - 0.261;
                vert.Radius = -1.0;
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] + 0.078;
                vert.Coordinates[2] = vert.Coordinates[2] - 0.243;
                vert.Coordinates[3] = vert.Coordinates[3] - 0.967;
                vert.Radius = -1.0;
                skinList.push_back(vert);
                break;
            }
            case 0:
            {
                Vertex vert(0.0, 0.0, 0.0, -1, -1, 0.0);
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] - 0.723;
                vert.Coordinates[2] = vert.Coordinates[2] - 1.474;
                vert.Coordinates[3] = vert.Coordinates[3] + 0.148;
                vert.Radius = -1.0;
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] + 0.834;
                vert.Coordinates[2] = vert.Coordinates[2] + 0.487;
                vert.Coordinates[3] = vert.Coordinates[3] - 0.261;
                vert.Radius = -1.0;
                skinList.push_back(vert);

                vert = skinList[skinList.size() - 1];
                vert.Coordinates[1] = vert.Coordinates[1] + 0.078;
                vert.Coordinates[2] = vert.Coordinates[2] - 0.243;
                vert.Coordinates[3] = vert.Coordinates[3] - 0.967;
                vert.Radius = -1.0;
                skinList.push_back(vert);
                break;
            }
        }

        assert(skinList.size() >= 4);
        fp = fopen("skin", "w");
        fprintf(fp, "%d\n", skinList.size());
        fprintf(fp, "#junk\n");
        for (uint ll = 0; ll < skinList.size(); ll++) {
            fprintf(fp, "%d %.3f %.3f %.3f %.3f\n", ll + 1, skinList[ll].Coordinates[1], skinList[ll].Coordinates[2],
                    skinList[ll].Coordinates[3], skinList[ll].Radius);
        }
        fclose(fp);

        skinList.clear();
        system("./smesh skin -s skin.off -t skin.tet");

        sprintf(commandLine, "mv skin_lev0.off %s/%d.off", folder, i);
        std::cerr << commandLine << std::endl;
        system(commandLine);
        sprintf(commandLine, "mv skin %s/%d.uob", folder, i);
        std::cerr << commandLine << std::endl;
        system(commandLine);
    }
}
