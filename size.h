/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef SIZE_H
#define SIZE_H

#include <cassert>
#include <gmp.h>
#include <vertex.h>
#include <edge.h>
#include <triangle.h>
#include <triangle.h>
#include <deluanaycomplex.h>

class Size {
public:
    Size();
    ~Size();

    int AlfRatioCompare(mpz_t* a, mpz_t* b, mpz_t* c, mpz_t* d);
    void VertSize(std::vector<Vertex> & vertexList, int i, mpz_t *p, mpz_t *q);
    int CheckVertex(std::vector<Vertex> & vertexList, int a, int b);
    void CheckEdge(mpz_t *p_mp, mpz_t *q_mp, mpz_t *r_mp, bool *isAttached);
    void EdgeSize(std::vector<Vertex> & vertexList, int a, int b, mpz_t *p, mpz_t *q);
    void TrigSize(std::vector<Vertex> & vertexList, int a, int b, int c, mpz_t *p, mpz_t *q);
    void CheckTriangle(mpz_t *p_mp, mpz_t *q_mp, mpz_t *r_mp, mpz_t *s_mp, bool *isAttached);
    void TetraSize(DeluanayComplex *delcx, std::vector<Vertex> & vertexList, int a, int b, int c, int d, mpz_t *p, mpz_t *q, int index);

private:
    int ISort3(int *a, int *b, int *c);
    int ISort4(int *a, int *b, int *c, int *d);
};

#endif // SIZE_H
