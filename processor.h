/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <deluanaycomplex.h>
#include <pocket.h>
#include <mouth.h>
#include <alphacomplex.h>
#include <volume.h>
#include <vertex.h>
#include <filereader.h>
#include <rankmap.h>
#include <simplexmasterlistmap.h>
#include <vector>

class SortablePair {
public:
    int index;
    int value;
    bool isEdge;

    SortablePair() {
        isEdge = false;
    }

    bool operator<(const SortablePair& rhs) const {
        if (value == rhs.value) return isEdge;
        return value < rhs.value;
    }
};

class Processor {
    //private:
public:
    std::vector<Vertex> vertexList;
    std::vector<int> PocketNMouths;
    AlphaComplex *alcx;
    FileReader *fr;
    Pocket *pocket;
    Mouth *mouth;
    Volume *volume;
    double scale;

    void MapPocketsToMouths();

    const char * crdFile;
    // parameters
    double alpha, solRad;

    //public:

    Processor();
    Processor(const char* crdFile, double solRad, double alpha);
    ~Processor();

    void read();
    int getMaxRank();
    int getMaxPersistence();
    int getRankForAlpha(double alphavalue);
    double getAlphaValue(int rank);
    void CalculateEverythingFor(int rank);
    void CalculateRelevant(int rank);
    void CalculateVolumes(int rank);

    int getNumberOfAtoms();
    int getNumberOfSimplices();

    bool isMouthTri(Triangle tri);

    void extractChannels();
    void writeAlphaComplex(const char* outFile);
};

#endif // PROCESSOR_H
