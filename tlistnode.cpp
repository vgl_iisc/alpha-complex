/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "tlistnode.h"

TlistNode::TlistNode(mpz_t *p, mpz_t *q, int rank, int index, int selin, unsigned int ft, unsigned int rt) {
    mpz_init(this->a);
    mpz_init(this->b);
    mpz_set(this->a, *p);
    mpz_set(this->b, *q);
    this->r = rank;
    this->ix = index;
    this->si = selin;
    this->ftype = ft;
    this->rtype = rt;
}

TlistNode::~TlistNode() {
    //mpz_clear(a);
    //mpz_clear(b);
}

/*!
    \fn TlistNode::operator < (const TlistNode& rhs )
 */
bool TlistNode::operator<(const TlistNode& rhs) const {
    mpz_t p;
    mpz_init(p);
    mpz_t q;
    mpz_init(q);
    mpz_t r;
    mpz_init(r);
    int temp;

    mpz_mul(p, this->a, rhs.b);
    mpz_mul(q, this->b, rhs.a);
    mpz_sub(r, p, q);

    temp = mpz_sgn(r);

    if (temp < 0) {
        mpz_clear(p);
        mpz_clear(q);
        mpz_clear(r);
        return true;
    } else {

        mpz_clear(p);
        mpz_clear(q);
        mpz_clear(r);
        return false;
    }
}

TlistNode TlistNode::operator=(TlistNode tl) {
    ftype = tl.ftype;
    rtype = tl.rtype;
    r = tl.r;
    ix = tl.ix;
    si = tl.si;
    mpz_init(a);
    mpz_init(b);
    mpz_set(a, tl.a);
    mpz_set(b, tl.b);

    return *this;
}
