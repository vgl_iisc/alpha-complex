LIBS *= -L/usr/lib \
    -lgmp 
TARGET = alphaComplex
TEMPLATE = app
SOURCES += main.cpp \
    processor.cpp \
    vertex.cpp \
    vector3.cpp \
    edge.cpp \
    triangle.cpp \
    tetrahedron.cpp \
    sos.cpp \
    alfmasternode.cpp \
    linktrig.cpp \
    tlistnode.cpp \
    node.cpp \
    deluanaycomplex.cpp \
    alphacomplex.cpp \
    mheapnode.cpp \
    size.cpp \
    disjointset.cpp \
    depth.cpp \
    deluanayflow.cpp \
    flow.cpp \
    filereader.cpp \
    pocket.cpp \
    mouth.cpp \
    spacefillmeasure.cpp \
    volume.cpp \
    trigvol.cpp \
    rankmap.cpp \
    simplexmasterlistmap.cpp \
    metric.cpp
HEADERS += \
    processor.h \
    vertex.h \
    vector3.h \
    edge.h \
    triangle.h \
    tetrahedron.h \
    sos.h \
    alfmasternode.h \
    linktrig.h \
    tlistnode.h \
    node.h \
    deluanaycomplex.h \
    alphacomplex.h \
    mheapnode.h \
    size.h \
    disjointset.h \
    depth.h \
    deluanayflow.h \
    flow.h \
    filereader.h \
    pocket.h \
    mouth.h \
    spacefillmeasure.h \
    volume.h \
    trigvol.h \
    rankmap.h \
    simplexmasterlistmap.h \
    metric.h
