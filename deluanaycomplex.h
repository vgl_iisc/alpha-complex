/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef DELUANAYCOMPLEX_H
#define DELUANAYCOMPLEX_H

#include <vector>
#include <cassert>
#include <algorithm>
#include <cmath>
#include <tetrahedron.h>
#include <triangle.h>
#include <edge.h>
#include <vertex.h>
#include <sos.h>
#include <gmp.h>
#include <vector3.h>

class LinkFacet {
public:
    int f1;
    int f2;
};

class LinkIndex {
public:
    int i1;
    int i2;
};

class DeluanayComplex {
private:
    static int inf4_1[5];
    static int sign4_1[5];
    static int inf4_2[5][5];
    static int sign4_2[5][5];
    static int sign4_3[5];
    static int inf5_2[5][5];
    static int sign5_2[5][5];
    static int inf5_3[5];
    static int sign5_3[5];
    static int order1[5][4];
    static int order2[7][3];
    static int order3[7][3];
    static int idxList[5][4];
    static int table32[4][4];
    static int table32_2[4][3];
    static int table41[4][4];
    static int table41_2[4][3];
    static int order[4][3];
    static int order_1[4][4];
    static int other[5][4];
    static int other2[5][5][3];
    static int idx_list[4][3];
    static int mask[7];
    static int pair[7][3];
    static int trig_info[7][3];
    static int trig_pos[7][3];
    static double eps;
    static int ir[98];
    static int iy;

    int nvertex;
    int ntetra;
    int iseed;
    int nkill, nfree;
    std::vector <int> free;
    std::vector <int> kill;
    std::vector <LinkFacet> linkFacet;
    std::vector <LinkIndex> linkIndex;
    std::vector <int> infCount;
    int ival, iredundant, tetraLoc, iff;
    std::vector <int> infPoint;
    Sos *sos;

    int compare(Vertex *a, Vertex *b);

    void ValSort2(int a, int b, int *ia, int *ib, int *iswap);
    void ValSort3(int a, int b, int c, int *ia, int *ib, int *ic, int *iswap);
    void ValSort4(int a, int b, int c, int d, int *ia, int *ib, int *ic, int *id, int *iswap);
    void ValSort5(int a, int b, int c, int d, int e, int *ia, int *ib, int *ic, int *id, int *ie, int *iswap);

    void TetraVol(std::vector<Vertex>& vertexList, int ia, int ib, int ic, int id, double *vol);

    void ISortIdx(int vertice[], int idx[], int *nswap, int *n);

    void EliminateDup(std::vector <Vertex> &sortedVertexList);
    void AddDummyLF();
    void InitFreeKill();

    void Jump(std::vector<Vertex> & vertexList, int *iseed, int ival, int *itetra);
    void InsideTetraJW(std::vector<Vertex> & vertexList, int p, int a, int b, int c, int d, int iorient, int *is_in, int *redundant, int *ifail);
    void LocateByJumpWalk(std::vector<Vertex> & vertexList, int *iseed, int ival, int *tetraLoc, int *iredundant);
    void FindTetra(int itetra, int idx_c, int a, int b, int o, int *ifind, int *tetra_loc, int *idx_a, int *idx_b);
    void DefineFacet(int itetra, int jtetra, int idx_o, int itouch[], int idx[], int jtouch[], int jdx[]);
    void RegularConvex(std::vector<Vertex> & vertexList, int a, int b, int c, int p, int o, int itest_abcp, bool *regular, bool *convex, bool *test_abpo, bool *test_bcpo, bool *test_capo);

    void FlipJW1_4(int ipoint, int itetra);
    void FlipJW(std::vector<Vertex> & vertexList);
    void FlipJW2_3(int itetra, int jtetra, int p, int o, int itetra_touch[], int itetra_idx[], int jtetra_touch[], int jtetra_idx[], bool *test_abpo, bool *test_bcpo, bool *test_capo, int *ierr);
    void FlipJW3_2(int itetra, int jtetra, int ktetra, int vertices[], int itetra_touch[], int itetra_idx[], int jtetra_touch[], int jtetra_idx[], int ktetra_touch[], int ktetra_idx[], bool *test_bcpo, bool *test_acpo, int *ierr);
    void FlipJW4_1(std::vector<Vertex>& vertexList, int itetra, int jtetra, int ktetra, int ltetra, int vertices[], int ishare, int idx, int jshare, int jdx, int kshare, int kdx, int lshare, int ldx, bool *test_acpo, int *ierr);

    void MissInfSign(int i, int j, int k, int *l, int *sign);
    void Peel(std::vector<Vertex> & vertexList);
    void MarkZero(int itetra, int ivertex);
    void RemoveInf();

    void CNInit();
    void CNClean();
    void MarkIterate(std::vector<Vertex> &vertexList);
    int MarkCH(std::vector<Vertex> &vertexList);
    void MarkNeighbours(int t, int n_iter);
    void MarkNeighboursInSet(int n_iter, int i_beg, int i_end);
    void MarkNotMarked();

public:
    int redundantCount;
    std::vector <Tetrahedron> DeluanayTet;
    std::vector <Triangle> DeluanayTrigs;
    std::vector <Edge> DeluanayEdges;

    DeluanayComplex();
    ~DeluanayComplex();

    double ran2(int *idum);
    void ConstructDT(std::vector<Vertex> & vertexList);
    void DefineTriangles();
    void DefineEdges(std::vector<Vertex> & vertexList);
    void CalculateNormals(std::vector<Vertex> &vertexList);
    void CorrectNormals(std::vector<Vertex> &vertexList);
};

#endif // DELUANAYCOMPLEX_H
