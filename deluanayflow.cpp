/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "deluanayflow.h"
#include <iostream>
using namespace std;

DeluanayFlow::DeluanayFlow() {
    flow = new Flow();
}

DeluanayFlow::~DeluanayFlow() {
}

/*!
    \fn DeluanayFlow::CalculateDF(DeluanayComplex *delcx, std::vector<Vertex> & vertexList)
 */
void DeluanayFlow::CalculateDF(DeluanayComplex *delcx, std::vector<Vertex> & vertexList) {
    int i, j, k, l, m, n, p;
    uint idx;
    int iflow[5];

    for (idx = 1; idx < delcx->DeluanayTet.size(); idx++) {
        if (delcx->DeluanayTet[idx].Status == 0) continue;

        i = delcx->DeluanayTet[idx].Corners[1];
        j = delcx->DeluanayTet[idx].Corners[2];
        k = delcx->DeluanayTet[idx].Corners[3];
        l = delcx->DeluanayTet[idx].Corners[4];

        for (m = 1; m <= 4; m++) {
            iflow[m] = 0;
            n = delcx->DeluanayTet[idx].Neighbours[m];
            p = delcx->DeluanayTet[idx].Nindex[m];
            if ((n != 0) && (n < idx)) {
                if (delcx->DeluanayTet[n].TetFlow[p] == 1)
                    iflow[m] = -1;
            }
        }

        flow->CalculateFlow(vertexList, i, j, k, l, iflow);

        for (m = 1; m <= 4; m++) {
            if (iflow[m] == 1) {
                delcx->DeluanayTet[idx].TetFlow[m] = 1;
            } else {
                delcx->DeluanayTet[idx].TetFlow[m] = 0;
            }
        }
    }
}

