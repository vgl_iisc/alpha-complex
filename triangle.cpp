/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "triangle.h"

Triangle::Triangle() {
    this->ReverseLink1 = 0;
    this->ReverseLink2 = 0;
    this->nLink = 0;
    this->isValid = false;

    for (int i = 0; i < 4; i++) {
        this->Corners[i] = -1;
        this->TrigLink[i] = -1;
    }
    this->Hull = -1;
    this->AlphaStatus = -1;
    this->IsAttached = false;
    this->Entry = -1;
    this->Rho = 0;
    this->Mu1 = 0;
    this->Mu2 = 0;
    this->Size = -1.0;
    this->ufKey = -1;
    this->PocIndex = -1;
    this->IsMouth = false;
    this->Repeats = -1;
    this->MainRepeats = -1;
    this->Persistence = -1;
    this->AlphaPersistence = 0.0;
    this->Normal = new Vector3(0.0, 0.0, 0.0);
}

Triangle::Triangle(unsigned int n[3]) {
    this->ReverseLink1 = 0;
    this->ReverseLink2 = 0;
    this->nLink = 0;
    this->isValid = false;

    this->Corners[1] = n[0];
    this->Corners[2] = n[1];
    this->Corners[3] = n[2];
    for (int i = 0; i < 4; i++) {
        this->TrigLink[i] = -1;
    }
    this->Hull = -1;
    this->AlphaStatus = -1;
    this->IsAttached = false;
    this->Entry = -1;
    this->Rho = 0;
    this->Mu1 = 0;
    this->Mu2 = 0;
    this->Size = -1.0;
    this->ufKey = -1;
    this->PocIndex = -1;
    this->IsMouth = false;
    this->Repeats = -1;
    this->MainRepeats = -1;
    this->Persistence = -1;
    this->AlphaPersistence = 0.0;
    this->Normal = new Vector3(0.0, 0.0, 0.0);
}

Triangle::~Triangle() {
}

Vector3 Triangle::getCentroid(std::vector<Vertex> & vertList) {
    Vector3 ret;
    Vector3 v1 = vertList[Corners[1]].getCoordVector();
    Vector3 v2 = vertList[Corners[2]].getCoordVector();
    Vector3 v3 = vertList[Corners[3]].getCoordVector();
    ret = v1;
    Vector3::Sum(&ret, &ret, &v2);
    Vector3::Sum(&ret, &ret, &v3);
    Vector3::Scale(&ret, &ret, 1.0 / 3.0);
    return ret;
}

