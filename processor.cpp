/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "processor.h"
#include <cstring>
#include <iostream>

Processor::Processor() {
    fr = new FileReader();
    alcx = 0;
    mouth = new Mouth();
}

Processor::Processor(const char* crdFile, double solRad, double alpha) {
    this->crdFile = crdFile;

    fr = new FileReader();
    alcx = NULL;
    mouth = new Mouth();

    this->solRad = solRad;
    this->alpha = alpha;
}

Processor::~Processor() {
}

using namespace std;

void Processor::read() {
    double min[3], max[3];

    fr->ReadVerticesCRDShort(crdFile, vertexList, min, max, solRad);

    alcx = new AlphaComplex(this, vertexList);
    alcx->BuildSpectrum();
    int alphaRank = alcx->AlphaRank(alpha);

    pocket = new Pocket(vertexList, alcx->delcx->DeluanayTrigs.size(), alcx->delcx->DeluanayEdges.size());
    //volume = new Volume(vertexList,vertexList.size(),alcx->delcx->DeluanayTrigs.size(),alcx->delcx->DeluanayEdges.size());
    if (alphaRank != 0 && alphaRank != alcx->MaximumRank) {
        alcx->BuildComplex(alphaRank, vertexList);
        pocket->FindPockets(alcx->delcx, alcx->sortedTet);
        std::vector< std::vector<int> > pockets = pocket->GetPockets();
        mouth->FindMouths(pockets, alcx->delcx);
        MapPocketsToMouths();
    }
}

/*!
    \fn Processor::getMaxRank()
 */
int Processor::getMaxRank() {
    return alcx->getMaxRank();
}

/*!
    \fn Processor::getRankForAlpha(double alphavalue)
 */
int Processor::getRankForAlpha(double alphavalue) {
    return alcx->AlphaRank(alphavalue);
}

/*!
    \fn Processor::getMaxPersistence()
 */
int Processor::getMaxPersistence() {
    return alcx->getMaxPersistence();
}

/*!
    \fn Processor::getAlphaValue(int rank)
 */
double Processor::getAlphaValue(int rank) {
    return alcx->AlphaThreshold(rank);
}

/*!
    \fn Processor::CalculateEverythingFor(int rank)
 */
void Processor::CalculateEverythingFor(int rank) {
    double real_alpha;
    if (alcx->MaximumRank != 0) {
        alcx->BuildComplex(rank, vertexList);
        real_alpha = alcx->AlphaThresholdSqr(rank);
        if (rank != 0) {
            alcx->Adjust(real_alpha, vertexList);
        }
        if (rank != 0 && rank != alcx->MaximumRank) {
            pocket->FindPockets(alcx->delcx, alcx->sortedTet);
            pocket->MeasurePockets(alcx->delcx, vertexList);
            //pocket->PocketProperties (alcx->delcx,vertexList,rank);
            std::vector<std::vector<int> > pockets = pocket->GetPockets();
            mouth->FindMouths(pockets, alcx->delcx);
            mouth->MeasureMouths(alcx->delcx, vertexList);
            volume->MeasureVolume(alcx->delcx, vertexList, 0);
            //volume->MeasureVolume(alcx,vertexList,totVol,totSurf,0);
            MapPocketsToMouths();
        }
    }
}

/*!
    \fn Processor::CalculateRelevant(int rank)
 */
void Processor::CalculateRelevant(int rank) {
    double real_alpha;
    if (alcx->MaximumRank != 0) {
        alcx->BuildComplex(rank, vertexList);
        real_alpha = alcx->AlphaThresholdSqr(rank);
        if (rank != 0) {
            alcx->Adjust(real_alpha, vertexList);
        }
        if (rank != 0 && rank != alcx->MaximumRank) {
            pocket->FindPockets(alcx->delcx, alcx->sortedTet);
            std::vector<std::vector<int> > pockets = pocket->GetPockets();
            mouth->FindMouths(pockets, alcx->delcx);
            MapPocketsToMouths();
        }
    }
}

/*!
    \fn Processor::CalculateVolumes(int rank)
 */
void Processor::CalculateVolumes(int rank) {
    //double real_alpha;
    if (alcx->MaximumRank != 0) {
        if (rank != 0 && rank != alcx->MaximumRank) {
            pocket->MeasurePockets(alcx->delcx, vertexList);
            //pocket->PocketProperties (alcx->delcx,vertexList,rank);
            mouth->MeasureMouths(alcx->delcx, vertexList);
            volume->MeasureVolume(alcx->delcx, vertexList, 0);
            //volume->MeasureVolume (alcx,vertexList,totVol,totSurf,0,rank);
            //volume->FindVolume (alcx,vertexList,totVol,totSurf,0,rank);
            //alcx->FindProperties (vertexList,totVol,totSurf,rank);
        }
    }
}

/*!
    \fn Processor::MapPocketsToMouths()
 */
void Processor::MapPocketsToMouths() {
    PocketNMouths.clear();
    PocketNMouths.reserve(pocket->npockets);

    for (int i = 0; i < pocket->npockets; i++) {
        PocketNMouths[i] = 0;
    }
    for (int i = 0; i < mouth->nmouths; i++) {
        int j = mouth->mouthPocket(i, alcx->delcx);
        PocketNMouths[j]++;
    }
    mouth->CleanUp(alcx->delcx);
}

int Processor::getNumberOfAtoms() {
    return vertexList.size();
}

int Processor::getNumberOfSimplices() {
    return (vertexList.size() + alcx->delcx->DeluanayEdges.size() + alcx->delcx->DeluanayTrigs.size() + alcx->delcx->DeluanayTet.size());
}

bool Processor::isMouthTri(Triangle tri) {
    bool isMouth = tri.nLink == 2 && (alcx->delcx->DeluanayTet[tri.ReverseLink1].Depth == (int) alcx->delcx->DeluanayTet.size() &&
            alcx->delcx->DeluanayTet[tri.ReverseLink2].Depth < (int) alcx->delcx->DeluanayTet.size());
    isMouth = isMouth || (tri.nLink == 2 && (alcx->delcx->DeluanayTet[tri.ReverseLink1].Depth < (int) alcx->delcx->DeluanayTet.size() &&
            alcx->delcx->DeluanayTet[tri.ReverseLink2].Depth == (int) alcx->delcx->DeluanayTet.size()));
    isMouth = isMouth || (tri.nLink == 1 && (alcx->delcx->DeluanayTet[tri.ReverseLink1].Depth < (int) alcx->delcx->DeluanayTet.size()));
    return isMouth;
}

void Processor::writeAlphaComplex(const char* outFile) {
    size_t i;
    FILE *fp = fopen(outFile, "w");
    int numEdges = 0, numTris = 0, numTets = 0;
    for (i = 0; i < alcx->delcx->DeluanayEdges.size(); i++) {
        Edge* edge = &alcx->delcx->DeluanayEdges[i];
        if (edge->AlphaStatus == 1) {
            numEdges++;
        }
    }
    for (i = 0; i < alcx->delcx->DeluanayTrigs.size(); i++) {
        Triangle* tri = &alcx->delcx->DeluanayTrigs[i];
        if (tri->AlphaStatus == 1) {
            numTris++;
        }
    }
    for (i = 0; i < alcx->delcx->DeluanayTet.size(); i++) {
        Tetrahedron* tet = &alcx->delcx->DeluanayTet[i];
        if (tet->AlphaStatus == 1) {
            numTets++;
        }
    }

    fprintf(fp, "%d %d %d %d\n", (int) vertexList.size(), numEdges, numTris, numTets);

    for (i = 0; i < vertexList.size(); i++) {
        Vertex* vert = &vertexList[i];
        fprintf(fp, "%9.5lf %9.5lf %9.5lf %9.5lf\n", vert->Coordinates[1], vert->Coordinates[2], vert->Coordinates[3], vert->Radius);
    }
    for (i = 0; i < alcx->delcx->DeluanayEdges.size(); i++) {
        Edge* edge = &alcx->delcx->DeluanayEdges[i];
        if (edge->AlphaStatus == 1) {
            fprintf(fp, "%9d %9d\n", edge->Corners[1], edge->Corners[2]);
        }
    }
    for (i = 0; i < alcx->delcx->DeluanayTrigs.size(); i++) {
        Triangle* tri = &alcx->delcx->DeluanayTrigs[i];
        if (tri->AlphaStatus == 1) {
            fprintf(fp, "%9d %9d %9d\n", tri->Corners[1], tri->Corners[2], tri->Corners[3]);
        }
    }
    for (i = 0; i < alcx->delcx->DeluanayTet.size(); i++) {
        Tetrahedron* tet = &alcx->delcx->DeluanayTet[i];
        if (tet->AlphaStatus == 1) {
            fprintf(fp, "%9d %9d %9d %9d\n", tet->Corners[1], tet->Corners[2], tet->Corners[3], tet->Corners[4]);
        }
    }
}
