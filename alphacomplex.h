/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef ALPHACOMPLEX_H
#define ALPHACOMPLEX_H

#define ALF_MAXIMUM  ((float) 1E+37)
#define ALF_MINIMUM  ((float) 1E-37)
#define ALF_ZERO     ((float) 0.0)
#define ALF_INFINITY ((float) 1E+37)

/* f_type/r_type enumerators; f_type == dimension + 1 */
#define ALF_TETRA    4
#define ALF_TRIANGLE 3
#define ALF_EDGE     2
#define ALF_VERTEX   1    /* ^                                       */
#define ALF_BLANK    0    /* |- 3 bits f_type  and  2 bits |- r_type */
#define ALF_RHO      1    /*                               V         */
#define ALF_MU1      2
#define ALF_MU2      3

#include <vector>
#include <algorithm>
#include <cmath>
#include <alfmasternode.h>
#include <mheapnode.h>
#include <tlistnode.h>
#include <deluanaycomplex.h>
#include <size.h>
#include <disjointset.h>
#include <depth.h>
#include <deluanayflow.h>
#include <rankmap.h>
#include <simplexmasterlistmap.h>
#include <volume.h>

class Processor;

class AlphaComplex {
private:
    //        AlfMasterNode *mlNode;
    std::vector<int> mlmarks;
    std::vector<int> mlrepeats;
    std::vector<int> betti0;
    Depth *depth;
    DeluanayFlow *dflow;
    DisJointSet *unionFind;
    std::vector<std::vector<int> > AllComponents;
    Volume *volume;
    double Epsilon;
    int LowRank;
    bool isFiltrationModified;
    Processor* processor;


    int MaxRank(int s, int r);
    int MinRank(int s, int r);

    int SoftInfinity(int last);
    int Add(std::vector <TlistNode> & sortedTListNode, int i, int last);
    void Put(mpz_t *p, mpz_t *q, int i, unsigned int ftype);
    void Push(int ix, uint fType, uint rType, int r);


    int MlEofSublist(int r);
    unsigned int MlRType();
    int MlPrev();
    int MlIsAttached();

    void SpectrumTetra(int i);
    void SpectrumTriangle(int i);
    void SpectrumEdge(int i);
    void SpectrumVertex(int i);

    void VertexMus();
    void EdgeMus();
    void TriangleMus();

    void CollectMaster();

    void MarkSimplices();
    void UpdateBoundaries();
    void PairSimplices();
    void UpdatePersistenceInfo();

    void CalculatePersistence();
    void WritePersistence();

    void fillCandidateTrigs(int lowRank, int highRank, std::vector <RankMap> &candidateTrigs);
    void fillCandidateTets(int lowRank, int highRank, std::vector <RankMap> &candidateTets);

public:
    AlfMasterNode *mlNode;
    long MaximumRank;
    int MaximumPersistence;
    double eps;
    int sp_j;
    int currentRank;
    int entries;
    int hmax;
    int hn;
    int max;
    int ranks;
    int vals;
    unsigned int ftype;
    unsigned int rtype;

    std::vector <double> Spectrum;
    std::vector <int> MasterList;
    std::vector <int> auxi;
    std::vector <AlfMasterNode> MasterNode;
    std::vector <TlistNode> tListNode;
    std::vector <MHeapNode> mHeapNode;
    std::vector <Vertex> vertexList;
    std::vector <int> sortedTet;
    std::vector <int> sortedTrigs;

    DeluanayComplex *delcx;
    Size *size;

    AlphaComplex(Processor* process, std::vector <Vertex> &vertlist);
    AlphaComplex(std::vector <Vertex> &vertlist, double center[], double * scale);
    ~AlphaComplex();

    double AlphaSqrt(double v);
    double AlphaThresholdSqr(int r);
    int AlphaRank(double v);
    int AlphaRankSqr(double v);
    double AlphaThreshold(int r);
    double RealValue(mpz_t *a, mpz_t *b);

    void SpectrumOpen();
    void Tetra();
    void Trigs();
    void Edges();
    void Vertices();
    void SpectrumClose();

    void BuildSpectrum();
    int getMaxRank();
    int getMaxPersistence();

    AlfMasterNode * GetMLNode();

    int MlSublist(int r);
    unsigned int MlFType();
    int MlNext();
    int MlIsFirst();

    void BuildComplex(int Rank, std::vector <Vertex> &vertlist);
    void Adjust(double real_alpha, std::vector <Vertex> &vertlist);

    bool InComplex(std::vector<Vertex> &vertexList, unsigned int ftype, int rank, int i);

    void PrintML();

    void FindProperties(std::vector<Vertex> &vertexList, int Rank);

    void MarkRelevantSimplices(double epsilon, int lowRank, int highRank, std::vector<RankMap> &candidateTrigs, std::vector<RankMap> &candidateTets);
    void ModifyFiltration(std::vector <SimplexMasterListMap> &refinedCandidateTrigs, std::vector <SimplexMasterListMap> &refinedCandidateTets);
    void UndoModifyFiltration(std::vector <SimplexMasterListMap> &refinedCandidateTrigs, std::vector <SimplexMasterListMap> &refinedCandidateTets);
};

#endif // ALPHACOMPLEX_H
