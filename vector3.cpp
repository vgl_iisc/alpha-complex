/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "vector3.h"

Vector3::Vector3(double x, double y, double z) {
    this->X = x;
    this->Y = y;
    this->Z = z;
}

Vector3::Vector3() {
    this->X = 0.0;
    this->Y = 0.0;
    this->Z = 0.0;
}

Vector3::~Vector3() {
}

/*!
    \fn Vector3::DotProduct(Vector3* v1, Vector3 *v2)
 */
void Vector3::DotProduct(Vector3* v1, Vector3 *v2, double *res) {
    *res = v1->X * v2->X + v1->Y * v2->Y + v1->Z * v2->Z;
}

/*!
    \fn Vector3::CrossProduct(Vector3 *result, Vector3 *u, Vector3 *v)
 */
void Vector3::CrossProduct(Vector3 *result, Vector3 *u, Vector3 *v) {
    result->X = u->Y * v->Z - u->Z * v->Y;
    result->Y = u->Z * v->X - u->X * v->Z;
    result->Z = u->X * v->Y - u->Y * v->X;
}

/*!
    \fn Vector3::DiffVector(Vector3 *result, Vector3 *u, Vector3 *v)
 */
void Vector3::DiffVector(Vector3 *result, Vector3 *u, Vector3 *v) {
    result->X = u->X - v->X;
    result->Y = u->Y - v->Y;
    result->Z = u->Z - v->Z;
}

/*!
    \fn Vector3::Normalize(Vector3 *result, Vector3 *v)
 */
void Vector3::Normalize(Vector3 *result, Vector3 *v) {
    double mod;
    DotProduct(v, v, &mod);
    mod = sqrt(mod);
    result->X = v->X / mod;
    result->Y = v->Y / mod;
    result->Z = v->Z / mod;
}

/*!
    \fn Vector3::Normalize()
 */
void Vector3::Normalize() {
    double mod;
    DotProduct(this, this, &mod);
    mod = sqrt(mod);
    this->X /= mod;
    this->Y /= mod;
    this->Z /= mod;
}

void Vector3::Sum(Vector3 *result, Vector3 *u, Vector3 *v) {
    result->X = u->X + v->X;
    result->Y = u->Y + v->Y;
    result->Z = u->Z + v->Z;
}

void Vector3::Scale(Vector3* result, Vector3 *in, double factor) {
    result->X = in->X * factor;
    result->Y = in->Y * factor;
    result->Z = in->Z * factor;
}

void Vector3::Distance(double *dist, Vector3 * u, Vector3 *v) {
    double sqrdist = 0.0;
    Vector3 D;
    DiffVector(&D, u, v);
    DotProduct(&D, &D, &sqrdist);
    if (sqrdist < 0.0) sqrdist = 0.0;
    *dist = sqrt(sqrdist);
}
