/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "tetrahedron.h"

#define Sign(X)  (((X) > 0) ? 1 : (((X) < 0) ? -1 : 0))

typedef double Matrix[4][4];

double det4(Matrix A) {
    double t0, t1, t2, t3;
    t0 = A[0][0] * (A[1][1] * (A[2][2] * A[3][3] - A[3][2] * A[2][3]) -
            A[2][1] * (A[1][2] * A[3][3] - A[3][2] * A[1][3]) +
            A[3][1] * (A[1][2] * A[2][3] - A[2][2] * A[1][3]));

    t1 = A[1][0] * (A[0][1] * (A[2][2] * A[3][3] - A[3][2] * A[2][3]) -
            A[2][1] * (A[0][2] * A[3][3] - A[3][2] * A[0][3]) +
            A[3][1] * (A[0][2] * A[2][3] - A[2][2] * A[0][3]));

    t2 = A[2][0] * (A[0][1] * (A[1][2] * A[3][3] - A[3][2] * A[1][3]) -
            A[1][1] * (A[0][2] * A[3][3] - A[3][2] * A[0][3]) +
            A[3][1] * (A[0][2] * A[1][3] - A[1][2] * A[0][3]));

    t3 = A[3][0] * (A[0][1] * (A[1][2] * A[2][3] - A[2][2] * A[1][3]) -
            A[1][1] * (A[0][2] * A[2][3] - A[2][2] * A[0][3]) +
            A[2][1] * (A[0][2] * A[1][3] - A[1][2] * A[0][3]));

    return (t0 - t1 + t2 - t3);
}

Tetrahedron::Tetrahedron() {
    Kill();
    for (int i = 0; i < 5; i++) {
        this->Corners[i] = 0;
        this->Neighbours[i] = 0;
        this->Nindex[i] = 0;
        this->TetLink[i] = -1;
        this->TetFlow[i] = 0;
    }
    this->AlphaStatus = -1;
    this->Index = -1;
    this->Orient = 0;
    this->Hull = -1;
    this->Depth = 0;
    this->Rho = 0;
    this->Size = -1.0;
    this->ufKey = -1;
    this->PocIndex = -1;
    this->posIndex = -1;
    this->Repeats = -1;
    this->MainRepeats = -1;
    this->Persistence = -1;
    this->AlphaPersistence = 0.0;
    this->isValid = false;
}

Tetrahedron::~Tetrahedron() {
}

/*!
    \fn Tetrahedron::Kill()
 */
void Tetrahedron::Kill() {
    this->Status = 0;
}

/*!
    \fn Tetrahedron::center4(std::vector<Vertex> &vertexList,std::vector<Vertex> &slist)
 */
void Tetrahedron::center4(std::vector<Vertex> &vertexList, std::vector<Vertex> &slist) {
    Vertex vert = powerVert(vertexList);
    slist.push_back(vert);
}

#include <iostream>

Vertex Tetrahedron::powerVert(std::vector<Vertex> &vertexList) {
    Matrix M;
    int i, j, k, l;
    double i0, j0, k0, l0;
    double i4, j4, k4, l4;
    double D0, Dx, Dy, Dz;
    double v[3] = {0, 0, 0}, W[4];
    double dx2[4], dy2[4], dz2[4], wi2[4];

    i = this->Corners[1];
    j = this->Corners[2];
    k = this->Corners[3];
    l = this->Corners[4];

    i4 = Sign(vertexList[i].BackRadius) * vertexList[i].BackRadius * vertexList[i].BackRadius;
    j4 = Sign(vertexList[j].BackRadius) * vertexList[j].BackRadius * vertexList[j].BackRadius;
    k4 = Sign(vertexList[k].BackRadius) * vertexList[k].BackRadius * vertexList[k].BackRadius;
    l4 = Sign(vertexList[l].BackRadius) * vertexList[l].BackRadius * vertexList[l].BackRadius;

    i0 = (1.0 / 2.0)*(i4 - pow(vertexList[i].Coordinates[1], 2.0) - pow(vertexList[i].Coordinates[2], 2.0) - pow(vertexList[i].Coordinates[3], 2.0));
    j0 = (1.0 / 2.0)*(j4 - pow(vertexList[j].Coordinates[1], 2.0) - pow(vertexList[j].Coordinates[2], 2.0) - pow(vertexList[j].Coordinates[3], 2.0));
    k0 = (1.0 / 2.0)*(k4 - pow(vertexList[k].Coordinates[1], 2.0) - pow(vertexList[k].Coordinates[2], 2.0) - pow(vertexList[k].Coordinates[3], 2.0));
    l0 = (1.0 / 2.0)*(l4 - pow(vertexList[l].Coordinates[1], 2.0) - pow(vertexList[l].Coordinates[2], 2.0) - pow(vertexList[l].Coordinates[3], 2.0));

    M[0][0] = vertexList[i].Coordinates[1];
    M[0][1] = vertexList[i].Coordinates[2];
    M[0][2] = vertexList[i].Coordinates[3];
    M[0][3] = 1.0;
    M[1][0] = vertexList[j].Coordinates[1];
    M[1][1] = vertexList[j].Coordinates[2];
    M[1][2] = vertexList[j].Coordinates[3];
    M[1][3] = 1.0;
    M[2][0] = vertexList[k].Coordinates[1];
    M[2][1] = vertexList[k].Coordinates[2];
    M[2][2] = vertexList[k].Coordinates[3];
    M[2][3] = 1.0;
    M[3][0] = vertexList[l].Coordinates[1];
    M[3][1] = vertexList[l].Coordinates[2];
    M[3][2] = vertexList[l].Coordinates[3];
    M[3][3] = 1.0;
    D0 = det4(M);

    M[0][0] = -i0;
    M[0][1] = vertexList[i].Coordinates[2];
    M[0][2] = vertexList[i].Coordinates[3];
    M[0][3] = 1.0;
    M[1][0] = -j0;
    M[1][1] = vertexList[j].Coordinates[2];
    M[1][2] = vertexList[j].Coordinates[3];
    M[1][3] = 1.0;
    M[2][0] = -k0;
    M[2][1] = vertexList[k].Coordinates[2];
    M[2][2] = vertexList[k].Coordinates[3];
    M[2][3] = 1.0;
    M[3][0] = -l0;
    M[3][1] = vertexList[l].Coordinates[2];
    M[3][2] = vertexList[l].Coordinates[3];
    M[3][3] = 1.0;
    Dx = det4(M);

    M[0][0] = vertexList[i].Coordinates[1];
    M[0][1] = -i0;
    M[0][2] = vertexList[i].Coordinates[3];
    M[0][3] = 1.0;
    M[1][0] = vertexList[j].Coordinates[1];
    M[1][1] = -j0;
    M[1][2] = vertexList[j].Coordinates[3];
    M[1][3] = 1.0;
    M[2][0] = vertexList[k].Coordinates[1];
    M[2][1] = -k0;
    M[2][2] = vertexList[k].Coordinates[3];
    M[2][3] = 1.0;
    M[3][0] = vertexList[l].Coordinates[1];
    M[3][1] = -l0;
    M[3][2] = vertexList[l].Coordinates[3];
    M[3][3] = 1.0;
    Dy = det4(M);

    M[0][0] = vertexList[i].Coordinates[1];
    M[0][1] = vertexList[i].Coordinates[2];
    M[0][2] = -i0;
    M[0][3] = 1.0;
    M[1][0] = vertexList[j].Coordinates[1];
    M[1][1] = vertexList[j].Coordinates[2];
    M[1][2] = -j0;
    M[1][3] = 1.0;
    M[2][0] = vertexList[k].Coordinates[1];
    M[2][1] = vertexList[k].Coordinates[2];
    M[2][2] = -k0;
    M[2][3] = 1.0;
    M[3][0] = vertexList[l].Coordinates[1];
    M[3][1] = vertexList[l].Coordinates[2];
    M[3][2] = -l0;
    M[3][3] = 1.0;
    Dz = det4(M);

    //center
    if (D0 == 0) {
        D0 = 1;
        std::cerr << "Determinant zero." << this->Index << " " << this->Status << std::endl;
    }
    v[0] = Dx / D0;
    v[1] = Dy / D0;
    v[2] = Dz / D0;

    dx2[0] = pow((v[0] - vertexList[i].Coordinates[1]), 2.0);
    dy2[0] = pow((v[1] - vertexList[i].Coordinates[2]), 2.0);
    dz2[0] = pow((v[2] - vertexList[i].Coordinates[3]), 2.0);
    wi2[0] = pow(vertexList[i].Radius, 2.0);

    dx2[1] = pow((v[0] - vertexList[j].Coordinates[1]), 2.0);
    dy2[1] = pow((v[1] - vertexList[j].Coordinates[2]), 2.0);
    dz2[1] = pow((v[2] - vertexList[j].Coordinates[3]), 2.0);
    wi2[1] = pow(vertexList[j].Radius, 2.0);

    dx2[2] = pow((v[0] - vertexList[k].Coordinates[1]), 2.0);
    dy2[2] = pow((v[1] - vertexList[k].Coordinates[2]), 2.0);
    dz2[2] = pow((v[2] - vertexList[k].Coordinates[3]), 2.0);
    wi2[2] = pow(vertexList[k].Radius, 2.0);

    dx2[3] = pow((v[0] - vertexList[l].Coordinates[1]), 2.0);
    dy2[3] = pow((v[1] - vertexList[l].Coordinates[2]), 2.0);
    dz2[3] = pow((v[2] - vertexList[l].Coordinates[3]), 2.0);
    wi2[3] = pow(vertexList[l].Radius, 2.0);

    for (int m = 0; m < 4; m++) {
        W[m] = dx2[m] + dy2[m] + dz2[m] - wi2[m];
    }

    if (W[0] < 0.0) {
        W[0] = 0.0;
    }
    W[0] = sqrt(W[0]);

    Vertex vert(v[0], v[1], v[2], W[0], -1, 0.0);
    return vert;
}
