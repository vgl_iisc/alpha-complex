/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "vertex.h"
#include <cmath>

Vertex::Vertex(double x, double y, double z, double radius, int index, double scale) {
    mpz_t temp1, temp2;

    this->Index = index;
    this->Coordinates[1] = this->NormCoordinates[1] = x;
    this->Coordinates[2] = this->NormCoordinates[2] = y;
    this->Coordinates[3] = this->NormCoordinates[3] = z;
    this->Radius = this->BackRadius = radius;
    this->Weight = this->BackWeight = pow(x, 2) + pow(y, 2) + pow(z, 2) - pow(radius, 2);
    for (int i = 1; i < 4; i++) {
        mpz_init(this->V[i]);
        mpz_set_d(this->V[i], this->Coordinates[i] * scale);
    }
    mpz_init(temp1);
    mpz_init(temp2);
    mpz_init(this->V[4]);

    mpz_set_d(temp1, this->Radius * (scale));
    mpz_mul(temp1, temp1, temp1);
    mpz_mul(temp2, this->V[3], this->V[3]), mpz_sub(temp1, temp2, temp1);
    mpz_mul(temp2, this->V[2], this->V[2]), mpz_add(temp1, temp2, temp1);
    mpz_mul(temp2, this->V[1], this->V[1]), mpz_add(this->V[4], temp2, temp1);

    mpz_clear(temp1);
    mpz_clear(temp2);
    this->Redinfo = 0;
    this->ranValue = 0;
    this->Hull = -1;
    this->AlphaStatus = -1;
    this->Coef = 1.0;
    this->Rho = 0;
    this->Mu1 = 0;
    this->Mu2 = 0;
    this->Repeats = -1;
    this->ufKey = -1;
    this->valid = true;
}

Vertex::~Vertex() {
}

/*!
    \fn Vertex::ranSortFunc(const Vertex& lhs,const Vertex& rhs)
 */
bool Vertex::ranSortFunc(const Vertex& lhs, const Vertex& rhs) {
    return (lhs.ranValue < rhs.ranValue);
}

/*!
    \fn Vertex::operator < (const Vertex& rhs ) const
 */
bool Vertex::operator<(const Vertex& rhs) const {
    return (Coordinates[0] < rhs.Coordinates[0]);
}
