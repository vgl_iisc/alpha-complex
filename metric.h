/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef METRIC_H
#define METRIC_H


#include <vector3.h>

#include <sos.h>
#include <vector>
#include <vertex.h>
#include <gmp.h>
#include <size.h>

typedef double Matrix[4][4];

class Metric {
private:
    double PI;
    std::vector<double> Radius2;
    Size *size;
    Sos *sos;
public:
    Metric(std::vector<Vertex> &vetexList, int vcount);
    ~Metric();

    double Det2(Matrix A);
    double Det3(Matrix A);
    double Det4(Matrix A);

    void Angle(double ang, Vector3* T, Vector3* u, Vector3* v);

    int Ccw(std::vector<Vertex> vertexList, int i, int j, int k, int l);

    Vector3 Center2(std::vector<Vertex> &vertexList, int i, int j);
    Vector3 Center3(std::vector<Vertex> &vertexList, int i, int j, int k);
    Vector3 Center4(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    Vector3 TriangleDual(std::vector<Vertex> &vertexList, int i, int j, int k);

    double AngleDihedral(Vector3 s, Vector3 t, Vector3 u, Vector3 v);
    double AngleSolid(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double DiskRadius(std::vector<Vertex> &vertexList, int i, int j);
    double DiskLength(std::vector<Vertex> &vertexList, int i, int j);
    double DiskArea(std::vector<Vertex> &vertexList, int i, int j);

    double SegmentHeight(std::vector<Vertex> &vertexList, int i, int j, int k);
    double SegmentAngle(std::vector<Vertex> &vertexList, int i, int j, int k);
    double SegmentLength(std::vector<Vertex> &vertexList, int i, int j, int k);
    double SegmentArea(std::vector<Vertex> &vertexList, int i, int j, int k);

    double Segment2Angle(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double Segment2Length(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double Segment2Area(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double CapHeight(std::vector<Vertex> &vertexList, int i, int j);
    double CapArea(std::vector<Vertex> &vertexList, int i, int j);
    double CapVolume(std::vector<Vertex> &vertexList, int i, int j);

    double Cap2Volume(std::vector<Vertex> &vertexList, int i, int j, int k);
    double Cap2Area(std::vector<Vertex> &vertexList, int i, int j, int k);

    double Cap3Volume(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double Cap3Area(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double SectorArea(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double SectorVolume(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double WedgeLength(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double WedgeArea(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double WedgeVolume(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double PawnLength(std::vector<Vertex> &vertexList, int i, int j, int k);
    double PawnArea(std::vector<Vertex> &vertexList, int i, int j, int k);
    double PawnVolume(std::vector<Vertex> &vertexList, int i, int j, int k);

    double BallVolume(std::vector<Vertex> &vertexList, int idx);
    double BallArea(int idx);

    double Ball2Length(std::vector<Vertex> &vertexList, int i, int j);
    double Ball2Volume(std::vector<Vertex> &vertexList, int i, int j);
    double Ball2Area(std::vector<Vertex> &vertexList, int i, int j);

    double Ball3Length(std::vector<Vertex> &vertexList, int i, int j, int k);
    double Ball3Volume(std::vector<Vertex> &vertexList, int i, int j, int k);
    double Ball3Area(std::vector<Vertex> &vertexList, int i, int j, int k);

    double Ball4Length(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double Ball4Volume(std::vector<Vertex> &vertexList, int i, int j, int k, int l);
    double Ball4Area(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    double TetraVolume(double a[], double b[], double c[], double d[]);
};

#endif // METRIC_H
