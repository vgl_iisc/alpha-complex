/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#ifndef VOLUME_H
#define VOLUME_H

#include <vector>
#include <cmath>
#include <spacefillmeasure.h>
#include <deluanaycomplex.h>
#include <trigvol.h>
#include <linktrig.h>
#include <metric.h>

class Volume {
private:
    double PI;
    //        AlfMasterNode* mlNode;

public:
    double TotVolume;
    double TotSurfaceArea;
    double Ssolv;
    double Vsolv;
    std::vector<double> IndVolume;
    std::vector<double> IndSurfArea;
    std::vector<TrigVol> DerivVol;
    std::vector<TrigVol> DerivSurf;

    std::vector<double> trig_eps; //(ntrig_max)
    std::vector<TrigVol> trig_sh; //(3,ntrig_max)
    std::vector<TrigVol> trig_dual1; //(3,ntrig_max)
    std::vector<TrigVol> trig_dual2; //(3,ntrig_max)
    std::vector<TrigVol> trig_ang;

    std::vector<int> nlink_trig;
    std::vector<LinkTrig> link_trig;

    std::vector<double> distpair; //(nedge_max)
    std::vector<double> distpair2; //(nedge_max)

    std::vector<double> edgeCoef;

    std::vector<double> Radius2;

    std::vector<double> CoefAsp;

    SpaceFillMeasure *spm;

    Metric *met;

    int test;

    Volume();
    Volume(std::vector<Vertex> &vertexList, int vcount, int trcount, int ecount);
    ~Volume();

    void VertexProperties(std::vector<Vertex> &vertexList, int idx, FILE *fp1);
    void EdgeProperties(std::vector<Vertex> &vertexList, int i, int j, FILE *fp1);
    void TriangleProperties(std::vector<Vertex> &vertexList, int i, int j, int k, FILE *fp1);
    void TetProperties(std::vector<Vertex> &vertexList, int i, int j, int k, int l, FILE *fp1);
    void FindTotal(std::vector<Vertex> &vertexList, double *TotVol, double *TotSurf);

    void DoTetraVertex(std::vector<Vertex> &verteList, int i, int j, int k, int l, int m, double *surf, double *vol);
    void DoTetraEdge(std::vector<Vertex> &vertexList, int i, int j, int k, int l, int m, double *surf, double *vol);
    void DoTetraTriangle(std::vector<Vertex> &vertexList, int i, int j, int k, int l, int m, double *surf, double *vol);
    double DoTetraVolume(std::vector<Vertex> &vertexList, int i, int j, int k, int l);

    void MeasureVolume(DeluanayComplex *delcx, std::vector<Vertex> & vertexList, int option);
};

#endif // VOLUME_H
