/*********************************************************************************
 *
 * Copyright (c) 2019 Visualization & Graphics Lab (VGL), Indian Institute of Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * Authors  : Raghavendra Sridharamurthy, Talha Bin Masood
 * Contact  : talha [AT] iisc.ac.in
 * Citations: 1. Sridharamurthy R. et al. "Extraction of robust voids and pockets in proteins."
 *               In "Visualization in Medicine and Life Sciences III." 
 *               Springer-Verlag, Mathematics and Visualization Series, 2016, 329-349
 *            2. Masood T. B. et al. "ChExVis: a tool for molecular channel extraction and visualization."
 *               BMC Bioinformatics, 2015, 16:119.  
 *********************************************************************************/

#include "filereader.h"

static double myMax(double a, double b) {
    if (b > a) return b;
    return a;
}

FileReader::FileReader() {
}

FileReader::~FileReader() {
}

void FileReader::ReadVerticesPQR(const char* filename, std::vector<Vertex> & vertexList, double center[],
        double *Scale, double min[], double max[], bool constantRadius, bool incrementRadius) {
    unsigned int n = 0;
    unsigned int i = 0;
    char *val;
    double r = 0, x = 0, y = 0, z = 0;
    double scale = 100000000.0;
    char line[100];
    char temp[10];
    int p;
    double charg;

    FILE* fp = fopen(filename, "r");
    assert(fp);

    val = fgets(line, 100, fp);
    sscanf(line, "%d", &n);
    val = fgets(line, 100, fp);
    Vertex vert(x, y, z, r, 0, scale);
    vertexList.push_back(vert);

    FILE *fp1 = fopen("radius.txt", "w");

    for (i = 1; i <= n; i++) {
        val = fgets(line, 100, fp);
        sscanf(line, "%s %d %s %s %d %lf %lf %lf %lf %lf", temp, &p, temp, temp, &p, &x, &y, &z, &charg, &r);
        fprintf(fp1, "%d %lf\t", i, r);
        if (incrementRadius) {
            r += 1.4;
        }
        if (constantRadius) {
            r = 0;
        }
        fprintf(fp1, "%lf\n", r);
        Vertex vert(x, y, z, r, i, scale);
        vertexList.push_back(vert);
    }
    fclose(fp1);
    fclose(fp);

    scale = 0;
    double maxx = vertexList[1].Coordinates[1], maxy = vertexList[1].Coordinates[2], maxz = vertexList[1].Coordinates[3];
    double minx = vertexList[1].Coordinates[1], miny = vertexList[1].Coordinates[2], minz = vertexList[1].Coordinates[3];
    double cx, cy, cz, w, h, e;

    for (i = 1; i < vertexList.size(); i++) {
        if (vertexList[i].Coordinates[1] > maxx) {
            maxx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[1] < minx) {
            minx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[2] > maxy) {
            maxy = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[2] < miny) {
            miny = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[3] > maxz) {
            maxz = vertexList[i].Coordinates[3];
        }
        if (vertexList[i].Coordinates[3] < minz) {
            minz = vertexList[i].Coordinates[3];
        }
    }

    // calculate model width, height, and depth
    w = (maxx) - (minx);
    h = (maxy) - (miny);
    e = (maxz) - (minz);

    // calculate center of the model
    cx = (maxx + minx) / 2.0;
    cy = (maxy + miny) / 2.0;
    cz = (maxz + minz) / 2.0;

    // calculate unitizing scale factor
    //scale = 2.0 / myMax(myMax(w, h), e);
    scale = myMax(myMax(w, h), e);

    center[0] = cx;
    center[1] = cy;
    center[2] = cz;
    *Scale = scale;

    min[0] = minx;
    min[1] = minx;
    min[2] = minx;
    max[0] = maxx;
    max[1] = maxy;
    max[2] = maxz;
}

#include "string.h";

void FileReader::ReadVerticesNew(const char* filename, std::vector<Vertex> & vertexList,
        double min[], double max[], double solRadius) {
    unsigned int n = 0;
    unsigned int i = 0;
    char *val;
    double r = 0, x = 0, y = 0, z = 0;
    int index, resID;
    double eps, sigma, charge, asp;
    double scale = 100000000.0;
    char line[200];
    char atom[10], resName[10], chain[10];


    FILE* fp = fopen(filename, "r");
    assert(fp);

    val = fgets(line, 200, fp);
    sscanf(line, "%d", &n);
    for (int i = 0; i < 5; i++) {
        val = fgets(line, 200, fp);
    }
    Vertex vert(x, y, z, r, 0, scale);
    vertexList.push_back(vert);

    FILE *fp1 = fopen("radius.txt", "w");

    for (i = 1; i <= n; i++) {
        val = fgets(line, 200, fp);
        sscanf(line, "%d %lf %lf %lf %lf %lf %lf %lf %lf %s %s %s %d",
                &index, &x, &y, &z, &r, &eps, &sigma, &charge, &asp, atom, resName, chain, &resID);
        fprintf(fp1, "%d %lf\t", i, r);
        r += solRadius;
        fprintf(fp1, "%lf\n", r);
        Vertex vert(x, y, z, r, i, scale);
        //                printf("%d %lf %lf %lf %lf %s %s %s %d\n", index, x, y, z, r,
        //                       vert.atom, vert.residue, vert.chain, vert.residueID);

        vertexList.push_back(vert);
    }
    fclose(fp1);
    fclose(fp);

    scale = 0;
    double maxx = vertexList[1].Coordinates[1], maxy = vertexList[1].Coordinates[2], maxz = vertexList[1].Coordinates[3];
    double minx = vertexList[1].Coordinates[1], miny = vertexList[1].Coordinates[2], minz = vertexList[1].Coordinates[3];
    double cx, cy, cz, w, h, e;

    for (i = 1; i < vertexList.size(); i++) {
        if (vertexList[i].Coordinates[1] > maxx) {
            maxx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[1] < minx) {
            minx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[2] > maxy) {
            maxy = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[2] < miny) {
            miny = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[3] > maxz) {
            maxz = vertexList[i].Coordinates[3];
        }
        if (vertexList[i].Coordinates[3] < minz) {
            minz = vertexList[i].Coordinates[3];
        }
    }

    // calculate model width, height, and depth
    w = (maxx) - (minx);
    h = (maxy) - (miny);
    e = (maxz) - (minz);

    // calculate center of the model
    cx = (maxx + minx) / 2.0;
    cy = (maxy + miny) / 2.0;
    cz = (maxz + minz) / 2.0;

    // calculate unitizing scale factor
    //scale = 2.0 / myMax(myMax(w, h), e);
    scale = myMax(myMax(w, h), e);

    min[0] = minx;
    min[1] = minx;
    min[2] = minx;
    max[0] = maxx;
    max[1] = maxy;
    max[2] = maxz;
}

void FileReader::ReadVerticesCRDShort(const char* filename, std::vector<Vertex> & vertexList,
        double min[], double max[], double solRadius) {
    unsigned int n = 0;
    unsigned int i = 0;
    char *val;
    double r = 0, x = 0, y = 0, z = 0;
    double scale = 100000000.0;
    char line[200];


    FILE* fp = fopen(filename, "r");
    assert(fp);

    val = fgets(line, 200, fp);
    sscanf(line, "%d", &n);
    val = fgets(line, 200, fp);

    Vertex vert(x, y, z, r, 0, scale);
    vertexList.push_back(vert);

    FILE *fp1 = fopen("radius.txt", "w");

    for (i = 1; i <= n; i++) {
        val = fgets(line, 200, fp);
        sscanf(line, "%lf %lf %lf %lf", &x, &y, &z, &r);
        fprintf(fp1, "%d %lf\t", i, r);
        r += solRadius;
        fprintf(fp1, "%lf\n", r);
        Vertex vert(x, y, z, r, i, scale);
        //                printf("%d %lf %lf %lf %lf %s %s %s %d\n", index, x, y, z, r,
        //                       vert.atom, vert.residue, vert.chain, vert.residueID);

        vertexList.push_back(vert);
    }
    fclose(fp1);
    fclose(fp);

    scale = 0;
    double maxx = vertexList[1].Coordinates[1], maxy = vertexList[1].Coordinates[2], maxz = vertexList[1].Coordinates[3];
    double minx = vertexList[1].Coordinates[1], miny = vertexList[1].Coordinates[2], minz = vertexList[1].Coordinates[3];
    double cx, cy, cz, w, h, e;

    for (i = 1; i < vertexList.size(); i++) {
        if (vertexList[i].Coordinates[1] > maxx) {
            maxx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[1] < minx) {
            minx = vertexList[i].Coordinates[1];
        }
        if (vertexList[i].Coordinates[2] > maxy) {
            maxy = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[2] < miny) {
            miny = vertexList[i].Coordinates[2];
        }
        if (vertexList[i].Coordinates[3] > maxz) {
            maxz = vertexList[i].Coordinates[3];
        }
        if (vertexList[i].Coordinates[3] < minz) {
            minz = vertexList[i].Coordinates[3];
        }
    }

    // calculate model width, height, and depth
    w = (maxx) - (minx);
    h = (maxy) - (miny);
    e = (maxz) - (minz);

    // calculate center of the model
    cx = (maxx + minx) / 2.0;
    cy = (maxy + miny) / 2.0;
    cz = (maxz + minz) / 2.0;

    // calculate unitizing scale factor
    //scale = 2.0 / myMax(myMax(w, h), e);
    scale = myMax(myMax(w, h), e);

    min[0] = minx;
    min[1] = minx;
    min[2] = minx;
    max[0] = maxx;
    max[1] = maxy;
    max[2] = maxz;
}
